# pylibgen-feedstock<br>
[![install with conda](https://anaconda.org/bluehood/pylibgen/badges/installer/conda.svg)](https://anaconda.org/bluehood/pylibgen)
[![last_built](https://anaconda.org/bluehood/pylibgen/badges/latest_release_date.svg)](https://anaconda.org/bluehood/pylibgen)
[![license](https://anaconda.org/bluehood/pylibgen/badges/license.svg)](https://anaconda.org/bluehood/pylibgen)

I am not related to pylibgen's development in any way, I just created a conda package for it.
Refer to the [project homepage](https://github.com/JoshuaRLi/pylibgen) for questions or bug reports.

## Conda installation
```bash
$ conda install -c bluehood pylibgen
```
